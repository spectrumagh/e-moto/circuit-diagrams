# Klonowanie repozytorium

W niniejszym repozytorium znajdują [submoduły](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

Wiąże się to z tym że należy zadbać aby się one sklonowały. Można to zrobić m.in:
- przy klonowaniu przez dodanie `--recurse-submodules`
- po sklonowaniu w folderze tego repozytorium przez wykonanie polecenia: `git submodule update --init`


---


W projekcie użyta jest biblioteka pobrana z internetu jej elementy znajdują się w folderze KiCADv6. Jej zawartość to:
-biblioteka schematów,
-biblioteka footprintów.
Należy samodzielnie dodać obie biblioteki do KiCada aby projekt działał poprawnie(poniżej instrukcja jak poprawnie dodać elementy).


==================================================================
TO DO:

* przejść checklistę komponentów w porównaniu z płytką pokazową
* sanity check użytych footprintów
* podmienić konektor wejściowy na pasujący do złącza ABS
==================================================================


Import Steps for Kicad (V6 and later)

Import Symbols

Using the *.kicad_sym file:
Extract the content of the downloaded *.zip file.
In KiCad, go to Preferences.
Click on Manage Symbol Libraries.
On the Global Libraries tab, click on Browse Libraries (the small folder icon)
Select the .kicad_sym file, then click Open.
The library will appear, click OK.
Click on Symbol Editor.
Type on the filter search field, and navigate to the symbol you imported.
Double-click over it to open the file.


Import Footprints

Using the *.kicad_mod file:
Extract the content of the downloaded *.zip file.
In KiCad, go to Preferences.
Click on Manage Footprint Libraries.
On the Global Libraries tab, click on Browse Libraries (the small folder icon)
Navigate to the Folder where the .kicad_mod file is located. Then click Select Folder.
Note: You will not normally see the .kicad_mod file on this step because you need to select the folder where it is located.

The library will appear, click OK.
Click on Footprint Editor.
Type on the filter search field, and navigate to the footprint you imported.
Double-click over it to open the file.
